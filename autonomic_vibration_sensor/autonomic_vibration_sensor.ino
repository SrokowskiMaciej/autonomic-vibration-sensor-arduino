//Add the SPI library so we can communicate with the ADXL345 sensor.
#include <SPI.h>
#include <SoftwareSerial.h>

// The pin powering LED indicating if device is sending data.
const int ledPin = 2;  
// Soft Serial interface Tx pin to communicate with bluetooth module.
const int softTx=9;
// Soft Serial interface Rx pin to communicate with bluetooth module.
const int softRx=8;

//Chip Select pin of Soft Serial interface.
int CS=10;

//Power Control Register of ADXL 345
char ADXL_345_POWER_CTL = 0x2D; 
//Data Format Register of ADXL 345
char ADXL_345_DATA_FORMAT = 0x31;
//Sample Rate Register of ADXL 345
char ADXL_345_SAMPLE_RATE = 0x2C;
//X-Axis Data 0
char DATAX0 = 0x32;
//X-Axis Data 1
char DATAX1 = 0x33;   
//Y-Axis Data 0
char DATAY0 = 0x34;  
//Y-Axis Data 1
char DATAY1 = 0x35;  
//Z-Axis Data 0
char DATAZ0 = 0x36; 
//Z-Axis Data 1
char DATAZ1 = 0x37;   


//Buffer holding single batch of values read from ADXL 345 Register
unsigned char values[10];
//Variable holding single sampole for each axis
int x,y,z;
//Soft Serial instance for communicating with BTM 222 by UART interface
SoftwareSerial softSerial(softRx, softTx);
//Constant telling device to start measuring and sending data
char START_MEASURING='a';
//Constant telling device to stop measuring and sending data
char STOP_MEASURING='s';
//Variable holding read char indicating if we should sand data
char measure;

void setup()
{
  //Initiate an SPI communication instance.
  SPI.begin();
  //Configure the SPI connection for the ADXL345.
  SPI.setDataMode(SPI_MODE3);
  /* Below code could be used to setup serial communication for sending data
   * to computers console for dubugging purposes:
   * Serial.begin(19200);
   */
  // initialize the ledPin as an output:
  pinMode(ledPin, OUTPUT);
  //Set Chip Select pin as an output to start read from ADXL 345.
  pinMode(CS, OUTPUT);

  //Setup Soft Serial pins
  pinMode(softTx, OUTPUT);
  pinMode(softRx, INPUT);

  //Start Soft Serial with 57600 baud rate
  softSerial.begin(57600);

  //Before communication starts, the Chip Select pin needs to be set high.
  digitalWrite(CS, HIGH);

  //Put the ADXL345 into 200 HZ sample rate by writing the value 0x0B5 to the ADXL_345_SAMPLE_RATE register.
  writeRegister(ADXL_345_SAMPLE_RATE, 0x0B);
  //Put the ADXL345 into +/- 4G range by writing the value 0x01 to the ADXL_345_DATA_FORMAT register.
  writeRegister(ADXL_345_DATA_FORMAT, 0x01);
  //Put the ADXL345 into Measurement Mode by writing 0x08 to the ADXL_345_POWER_CTL register.
  writeRegister(ADXL_345_POWER_CTL, 0x08);
}

void loop() {

  if (softSerial.available() > 0) {
    //Read if we should measure and send data or no
    measure = softSerial.read();
  }

  switch (measure) {
  case START_MEASURING:
    //If measuring light up LED
    digitalWrite(ledPin, HIGH);
    //Read batch of data
    readRegister(DATAX0, 6, values);

    //Combine values read from ADXL 345 to get one value from every two bytes
    //The X value is stored in values[0] and values[1].
    x = ((int)values[1]<<8)|(int)values[0];
    //The Y value is stored in values[2] and values[3].
    y = ((int)values[3]<<8)|(int)values[2];
    //The Z value is stored in values[4] and values[5].
    z = ((int)values[5]<<8)|(int)values[4];


    /*
     Below code could be uesd for debugging with computer console.
     Serial.print('x');
     Serial.print(x);
     Serial.print('y');
     Serial.print(y);
     Serial.print('z');
     Serial.print(z);
     */

    //Write values by UART interface to bluetooth module
    softSerial.print('x');
    softSerial.print(x);
    softSerial.print('y');
    softSerial.print(y);
    softSerial.print('z');
    softSerial.print(z);
    //Artificially creat 200 Hz reading frequency.
    //This should be done with interrupts in final version of a device
    delay(5); 
    break;
  case STOP_MEASURING:
    //If not measuring put out LED
    digitalWrite(ledPin, LOW);
    break;
  default:
    break;
  }

}

//This function will write a value to a register on the ADXL345.
//Parameters:
//  char registerAddress - The register to write a value to
//  char value - The value to be written to the specified register.
void writeRegister(char registerAddress, char value){
  //Set Chip Select pin low to signal the beginning of an SPI packet.
  digitalWrite(CS, LOW);
  //Transfer the register address over SPI.
  SPI.transfer(registerAddress);
  //Transfer the desired register value over SPI.
  SPI.transfer(value);
  //Set the Chip Select pin high to signal the end of an SPI packet.
  digitalWrite(CS, HIGH);
}

//This function will read a certain number of registers starting from a specified address and store their values in a buffer.
//Parameters:
//  char registerAddress - The register addresse to start the read sequence from.
//  int numBytes - The number of registers that should be read.
//  char * values - A pointer to a buffer where the results of the operation should be stored.
void readRegister(char registerAddress, int numBytes, unsigned char * values){
  //Since we're performing a read operation, the most significant bit of the register address should be set.
  char address = 0x80 | registerAddress;
  //If we're doing a multi-byte read, bit 6 needs to be set as well.
  if(numBytes > 1)address = address | 0x40;

  //Set the Chip select pin low to start an SPI packet.
  digitalWrite(CS, LOW);
  //Transfer the starting register address that needs to be read.
  SPI.transfer(address);
  //Continue to read registers until we've read the number specified, storing the results to the input buffer.
  for(int i=0; i<numBytes; i++){
    values[i] = SPI.transfer(0x00);
  }
  //Set the Chips Select pin high to end the SPI packet.
  digitalWrite(CS, HIGH);
}















