//Add the SPI library so we can communicate with the ADXL345 sensor
#include <SPI.h>
#include <SoftwareSerial.h>

const int ledPin = 2;  // the pin that the LED is attached to
const int softTx=9;
const int softRx=8;


//Assign the Chip Select signal to pin 10.
int CS=10;

//This is a list of some of the registers available on the ADXL345.
//To learn more about these and the rest of the registers on the ADXL345, read the datasheet!
char ADXL_345_POWER_CTL = 0x2D;    //Power Control Register
char ADXL_345_DATA_FORMAT = 0x31;
char ADXL_345_SAMPLE_RATE = 0x2C;
char DATAX0 = 0x32;   //X-Axis Data 0
char DATAX1 = 0x33;   //X-Axis Data 1
char DATAY0 = 0x34;   //Y-Axis Data 0
char DATAY1 = 0x35;   //Y-Axis Data 1
char DATAZ0 = 0x36;   //Z-Axis Data 0
char DATAZ1 = 0x37;   //Z-Axis Data 1


//This buffer will hold values read from the ADXL345 registers.
unsigned char values[10];
//These variables will be used to hold the x,y and z axis accelerometer values.
int x,y,z;
SoftwareSerial softSerial(softRx, softTx);
char serialA;
void setup()
{
  delay(1000);
   //Initiate an SPI communication instance.
  SPI.begin();
  delay(1000);
  //Configure the SPI connection for the ADXL345.
  SPI.setDataMode(SPI_MODE3);
  // initialize the ledPin as an output:
  pinMode(ledPin, OUTPUT);
  //Set up the Chip Select pin to be an output from the Arduino.
  pinMode(CS, OUTPUT);
  
  pinMode(softTx, OUTPUT);
  pinMode(softRx, INPUT);
  
  softSerial.begin(19200);
  delay(1000);
  //Before communication starts, the Chip Select pin needs to be set high.
  digitalWrite(CS, HIGH);

  //Put the ADXL345 into 800 HZ sample rate by writing the value 0x0D to the ADXL_345_SAMPLE_RATE register.
  //writeRegister(ADXL_345_SAMPLE_RATE, 0x0D);
  //Put the ADXL345 into +/- 4G range by writing the value 0x01 to the ADXL_345_DATA_FORMAT register.
  writeRegister(ADXL_345_DATA_FORMAT, 0x01);
  //Put the ADXL345 into Measurement Mode by writing 0x08 to the ADXL_345_POWER_CTL register.
  writeRegister(ADXL_345_POWER_CTL, 0x08);  //Measurement mode 
  
}

void loop() {

  if (softSerial.available() > 0) {
    serialA = softSerial.read();
  }


  switch (serialA) {
  case 'a':
    digitalWrite(ledPin, HIGH);
    readRegister(DATAX0, 6, values);

    //The ADXL345 gives 10-bit acceleration values, but they are stored as bytes (8-bits). To get the full value, two bytes must be combined for each axis.
    //The X value is stored in values[0] and values[1].
    x = ((int)values[1]<<8)|(int)values[0];
    //The Y value is stored in values[2] and values[3].
    y = ((int)values[3]<<8)|(int)values[2];
    //The Z value is stored in values[4] and values[5].
    z = ((int)values[5]<<8)|(int)values[4];

    softSerial.print('x');
    softSerial.print(x);
    softSerial.print('y');
    softSerial.print(y);
    softSerial.print('z');
    softSerial.print(z);

    delay(10); 
    break;
  case 's':
    digitalWrite(ledPin, LOW);
    break;
  default:

    break;
  }

}

//This function will write a value to a register on the ADXL345.
//Parameters:
//  char registerAddress - The register to write a value to
//  char value - The value to be written to the specified register.
void writeRegister(char registerAddress, char value){
  //Set Chip Select pin low to signal the beginning of an SPI packet.
  digitalWrite(CS, LOW);
  //Transfer the register address over SPI.
  SPI.transfer(registerAddress);
  //Transfer the desired register value over SPI.
  SPI.transfer(value);
  //Set the Chip Select pin high to signal the end of an SPI packet.
  digitalWrite(CS, HIGH);
}

//This function will read a certain number of registers starting from a specified address and store their values in a buffer.
//Parameters:
//  char registerAddress - The register addresse to start the read sequence from.
//  int numBytes - The number of registers that should be read.
//  char * values - A pointer to a buffer where the results of the operation should be stored.
void readRegister(char registerAddress, int numBytes, unsigned char * values){
  //Since we're performing a read operation, the most significant bit of the register address should be set.
  char address = 0x80 | registerAddress;
  //If we're doing a multi-byte read, bit 6 needs to be set as well.
  if(numBytes > 1)address = address | 0x40;

  //Set the Chip select pin low to start an SPI packet.
  digitalWrite(CS, LOW);
  //Transfer the starting register address that needs to be read.
  SPI.transfer(address);
  //Continue to read registers until we've read the number specified, storing the results to the input buffer.
  for(int i=0; i<numBytes; i++){
    values[i] = SPI.transfer(0x00);
  }
  //Set the Chips Select pin high to end the SPI packet.
  digitalWrite(CS, HIGH);
}









